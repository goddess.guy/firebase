import {useEffect} from 'react';
import messaging from '@react-native-firebase/messaging';

const SetNotification = () => {
  useEffect(() => {
    requestUserPermission();
  }, []);

  const requestUserPermission = async () => {
    const authStatus = await messaging().requestPermission();
    // const dd = await messaging().hasPermission();

    // console.log(authStatus, dd);

    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      const fcmToken = await messaging().getToken();

      if (fcmToken) {
        console.log('FCM TOKEN', fcmToken);
      } else {
        console.log('No FCM token received');
      }
      // console.log('FCM authorization status:', authStatus);
    }
  };
};

export default SetNotification;
