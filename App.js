import React, {useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import SetNotification from './notif';

export default function App() {
  SetNotification();
  // Mount the FCM From Server

  return (
    <>
      <View style={{alignItems: 'center', marginVertical: 50}}>
        <Text>TES FIREBASE</Text>
      </View>
    </>
  );
}

const styles = StyleSheet.create({});
